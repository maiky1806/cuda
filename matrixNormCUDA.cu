/* Matrix normalization.
 * Compile with "gcc matrixNorm.c" 
 */

/* ****** ADD YOUR CODE AT THE END OF THIS FILE. ******
 * You need not submit the provided code.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <sys/types.h>
#include <sys/times.h>
#include <sys/time.h>
#include <time.h>

/* Program Parameters */
#define MAXN 8000  /* Max value of N */
int N;  /* Matrix size */

/* Matrices */
volatile float A[MAXN][MAXN], B[MAXN][MAXN];

/* junk */
#define randm() 4|2[uid]&3

/* Prototype */
void matrixNorm();

/* returns a seed for srand based on the time */
unsigned int time_seed() {
  struct timeval t;
  struct timezone tzdummy;

  gettimeofday(&t, &tzdummy);
  return (unsigned int)(t.tv_usec);
}

/* Set the program parameters from the command-line arguments */
void parameters(int argc, char **argv) {
  int seed = 0;  /* Random seed */
  char uid[32]; /*User name */

  /* Read command-line arguments */
  srand(time_seed());  /* Randomize */

  if (argc == 3) {
    seed = atoi(argv[2]);
    srand(seed);
    printf("Random seed = %i\n", seed);
  } 
  if (argc >= 2) {
    N = atoi(argv[1]);
    if (N < 1 || N > MAXN) {
      printf("N = %i is out of range.\n", N);
      exit(0);
    }
  }
  else {
    printf("Usage: %s <matrix_dimension> [random seed]\n",
           argv[0]);    
    exit(0);
  }

  /* Print parameters */
  printf("\nMatrix dimension N = %i.\n", N);
}

/* Initialize A and B*/
void initialize_inputs() {
  int row, col;

  printf("\nInitializing...\n");
  for (col = 0; col < N; col++) {
    for (row = 0; row < N; row++) {
      A[row][col] = (float)rand() / 32768.0;
      B[row][col] = 0.0;
    }
  }

}

/* Print input matrices */
void print_inputs() {
  int row, col;

  if (N < 10) {
    printf("\nA =\n\t");
    for (row = 0; row < N; row++) {
      for (col = 0; col < N; col++) {
	    printf("%5.2f%s", A[row][col], (col < N-1) ? ", " : ";\n\t");
      }
    }
  }
}

void print_B() {
    int row, col;

    if (N < 10) {
        printf("\nB =\n\t");
        for (row = 0; row < N; row++) {
            for (col = 0; col < N; col++) {
                printf("%1.10f%s", B[row][col], (col < N-1) ? ", " : ";\n\t");
            }
        }
    }
}

int main(int argc, char **argv) {
  /* Timing variables */
  struct timeval etstart, etstop;  /* Elapsed times using gettimeofday() */
  struct timezone tzdummy;
  clock_t etstart2, etstop2;  /* Elapsed times using times() */
  unsigned long long usecstart, usecstop;
  struct tms cputstart, cputstop;  /* CPU times for my processes */

  /* Process program parameters */
  parameters(argc, argv);

  /* Initialize A and B */
  initialize_inputs();

  /* Print input matrices */
  print_inputs();

  /* Start Clock */
  printf("\nStarting clock.\n");
  gettimeofday(&etstart, &tzdummy);
  etstart2 = times(&cputstart);

  /* Gaussian Elimination */
  matrixNorm();

  /* Stop Clock */
  gettimeofday(&etstop, &tzdummy);
  etstop2 = times(&cputstop);
  printf("Stopped clock.\n");
  usecstart = (unsigned long long)etstart.tv_sec * 1000000 + etstart.tv_usec;
  usecstop = (unsigned long long)etstop.tv_sec * 1000000 + etstop.tv_usec;

  /* Display output */
  print_B();

  /* Display timing results */
  printf("\nElapsed time = %g ms.\n",
	 (float)(usecstop - usecstart)/(float)1000);

  printf("(CPU times are accurate to the nearest %g ms)\n",
	 1.0/(float)CLOCKS_PER_SEC * 1000.0);
  printf("My total CPU time for parent = %g ms.\n",
	 (float)( (cputstop.tms_utime + cputstop.tms_stime) -
		  (cputstart.tms_utime + cputstart.tms_stime) ) /
	 (float)CLOCKS_PER_SEC * 1000);
  printf("My system CPU time for parent = %g ms.\n",
	 (float)(cputstop.tms_stime - cputstart.tms_stime) /
	 (float)CLOCKS_PER_SEC * 1000);
  printf("My total CPU time for child processes = %g ms.\n",
	 (float)( (cputstop.tms_cutime + cputstop.tms_cstime) -
		  (cputstart.tms_cutime + cputstart.tms_cstime) ) /
	 (float)CLOCKS_PER_SEC * 1000);
      /* Contrary to the man pages, this appears not to include the parent */
  printf("--------------------------------------------\n");
  
  exit(0);
}

/* ------------------ Above Was Provided --------------------- */

/****** You will replace this routine with your own parallel version *******/
/* Provided global variables are MAXN, N, A[][] and B[][],
 * defined in the beginning of this code.  B[][] is initialized to zeros.
 */
 #define THREADSXBLOCK 256
 #define CHECK_ERR(x)                                    \
  if (x != cudaSuccess) {                               \
    fprintf(stderr,"%s in %s at line %d\n",             \
      cudaGetErrorString(err),__FILE__,__LINE__); \
    exit(-1);           \
  }                                                     \

__global__ void sumCol(float* d_Col, float* result_per_block, int N_size){
  __shared__ float partialSum[THREADSXBLOCK];
  int tID = blockDim.x * blockIdx.x + threadIdx.x;
  if(tID < N_size)
    partialSum[threadIdx.x] = d_Col[tID];

  __syncthreads();

  for(int i=blockDim.x>>1; i>0; i>>=1){
    if(threadIdx.x<i && (tID+i) < N_size)
      partialSum[threadIdx.x] += partialSum[threadIdx.x+i];
    __syncthreads();
  }
  if(threadIdx.x==0)
    result_per_block[blockIdx.x] = partialSum[0];

}

__global__ void devCol(float* d_Col, float* result_per_block, int N_size, float mu){
  __shared__ float partialSum[THREADSXBLOCK];
  int tID = blockDim.x * blockIdx.x + threadIdx.x;

  //Compute one value
  if (tID < N_size)
    partialSum[threadIdx.x] = powf(d_Col[tID] - mu, 2.0);

  __syncthreads();
  //Compute the sumation
  for(int i=blockDim.x>>1; i>0; i>>=1){
    if(threadIdx.x<i && (tID+i) < N_size)
      partialSum[threadIdx.x] += partialSum[threadIdx.x+i];
    __syncthreads();
  }
  if(threadIdx.x==0)
    result_per_block[blockIdx.x] = partialSum[0];

}

__global__ void sumBlocksAndDivide(float* result_per_block, int numBlocks, float* d_variable, int N){
  //Compute the sumation
  for(int i=numBlocks>>1; i>0; i>>=1){
    if(threadIdx.x<i && threadIdx.x < numBlocks)
      result_per_block[threadIdx.x] += result_per_block[threadIdx.x+i];
    __syncthreads();
  }
  if(threadIdx.x==0)
    *d_variable = result_per_block[0]/N;

}

__global__ void normCol(float* d_Col, int N_size, float mu, float sigma){
  int tID = blockDim.x * blockIdx.x + threadIdx.x;
  //Compute one value
  if (tID < N_size){
    if (sigma == 0.0)
        d_Col[tID] = 0.0;
    else
        d_Col[tID] = (d_Col[tID] - mu) / sigma;

  }
    

}
#define DEBUGTIME 0

void matrixNorm() {
  struct timeval etstart, etstop;  /* Elapsed times using gettimeofday() */
  struct timezone tzdummy;
  clock_t etstart2, etstop2;  /* Elapsed times using times() */
  unsigned long long usecstart, usecstop;
  struct tms cputstart, cputstop;  /* CPU times for my processes */

  int row, col; 
  float mu, sigma; // Mean and Standard Deviation
  cudaError_t err;

  int threadsPerBlock = 256;
  int blocksPerGrid = ceil(N/(float)threadsPerBlock);
  //Array with the present column
  float h_Col[N];

  //CUDA MEMORY VARIABLES
  //Array to copy the present column
  float* d_Col;
  err = cudaMalloc((void **) &d_Col, sizeof(float)*N);
  CHECK_ERR(err);

  //Array to take back the partial block sumations
  float* result_per_block;
  err = cudaMalloc((void **) &result_per_block, sizeof(float)*blocksPerGrid);
  CHECK_ERR(err);

  //Variables in CUDA memory
  float* d_mu;
  err = cudaMalloc((void **) &d_mu, sizeof(float));
  CHECK_ERR(err);

  float* d_sigma;
  err = cudaMalloc((void **) &d_sigma, sizeof(float));
  CHECK_ERR(err);

  printf("Computing with CUDA.\n");

    for (col=0; col < N; col++) {
      for (row=0; row < N; row++)
          h_Col[row]=A[row][col];
        if(DEBUGTIME == 1){
        /* Start Clock */
          printf("\nStarting clock.\n");
          gettimeofday(&etstart, &tzdummy);
          etstart2 = times(&cputstart);
        }
        err = cudaMemcpy(d_Col, h_Col, sizeof(float)*N, cudaMemcpyHostToDevice);
        CHECK_ERR(err);
        sumCol<<<blocksPerGrid,threadsPerBlock>>>(d_Col, result_per_block, N);
        sumBlocksAndDivide<<<1, blocksPerGrid>>>(result_per_block, blocksPerGrid, d_mu, N);

        //Check value
        // mu=0.0;
        // err = cudaMemcpy(&mu, d_mu, sizeof(float), cudaMemcpyDeviceToHost);
        // CHECK_ERR(err);
        // printf("CUDA mu: %f\n", mu);

        //Compute sigma
        devCol<<<blocksPerGrid,threadsPerBlock>>>(d_Col, result_per_block, N, mu);
        sumBlocksAndDivide<<<1, blocksPerGrid>>>(result_per_block, blocksPerGrid, d_sigma, N);

        //Check value
        // sigma=0.0;
        // err = cudaMemcpy(&sigma, d_sigma, sizeof(float), cudaMemcpyDeviceToHost);
        // CHECK_ERR(err);
        // printf("CUDA sigma: %f\n", sigma);


        normCol<<<blocksPerGrid,threadsPerBlock>>>(d_Col, N, mu, sigma);
        err = cudaMemcpy(h_Col, d_Col, sizeof(float)*N, cudaMemcpyDeviceToHost);
        CHECK_ERR(err);
        if(DEBUGTIME == 1){
          /* Stop Clock */
          gettimeofday(&etstop, &tzdummy);
          etstop2 = times(&cputstop);
          printf("Stopped clock.\n");
          usecstart = (unsigned long long)etstart.tv_sec * 1000000 + etstart.tv_usec;
          usecstop = (unsigned long long)etstop.tv_sec * 1000000 + etstop.tv_usec;
          printf("\nElapsed time = %g ms.\n", (float)(usecstop - usecstart)/(float)1000);
        }

        for (row=0; row < N; row++)
          B[row][col]=h_Col[row];
    }
    cudaFree(d_Col);
    cudaFree(result_per_block);
    cudaFree(d_mu);
    cudaFree(d_sigma);

}